//
//  Home+TableView.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import Foundation
import UIKit
extension Home: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apiNetworkResponseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HomeCell = tableView.dequeueCell(for: indexPath)
        cell.configureCell(data: apiNetworkResponseData[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return .init(150)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = HomeDetails(data: apiNetworkResponseData[indexPath.row])
        self.show(viewController, sender: nil)
    }
    
}
