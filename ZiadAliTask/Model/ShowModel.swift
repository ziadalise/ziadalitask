//
//  ShowModel.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import Foundation
// MARK: - WelcomeElement
struct DataModel: Codable {
    let score: Double?
    let show: Show?
}

// MARK: - Show
struct Show: Codable {
    let id: Int?
    let name: String?
    let image: Image?
    let rating: Rating?
    let url: String?
    let runtime: Int?
    let premiered: String?
    let averageRuntime: Int?
    let links: Links?
    let language: String?
    let type: String?
    let network: Network?
    let summary: String?

    enum CodingKeys: String, CodingKey {
        case id, name,image, rating, runtime, averageRuntime, premiered, url, language, type, network, summary
        case links = "_links"
    }
}



// MARK: - Image
struct Image: Codable {
    let medium, original: String?
}

struct Network: Codable{
    let id: Int?
    let name: String?
    let country: Country?
}
struct Country: Codable{
    let name, code, timezone: String?
}
// MARK: - Links
struct Links: Codable {
    let linksSelf, previousepisode: Previousepisode?

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case previousepisode
    }
}

// MARK: - Previousepisode
struct Previousepisode: Codable {
    let href: String?
}

// MARK: - Rating
struct Rating: Codable {
    let average: Int?
}

// MARK: - Schedule
struct Schedule: Codable {
    let time: String?
    let days: [String]?
}

enum Status: String, Codable {
    case ended = "Ended"
}

enum TypeEnum: String, Codable {
    case animation = "Animation"
    case documentary = "Documentary"
    case scripted = "Scripted"
}

