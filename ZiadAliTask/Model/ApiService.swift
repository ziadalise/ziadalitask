//
//  ApiService.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import Foundation

class ApiService{
    private var dataTask: URLSessionDataTask?
    
    func getData(completion: @escaping (Result<[DataModel] ,Error>) -> Void){
        let url = URL(string: "http://api.tvmaze.com/search/shows?q=Future")
        guard let url = url else {return}
        
        dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let response = response as? HTTPURLResponse else {
                print("empty response")
                return
            }
            print(response.statusCode)
            guard let data = data else {return}
            do{
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([DataModel].self, from: data)
                DispatchQueue.main.async {
                    completion(.success(jsonData))
                }
            }catch let error{
                completion(.failure(error))
            }
            
        }
        dataTask?.resume()
    }
}
