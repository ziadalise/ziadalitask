//
//  HomeDetails.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import UIKit

class HomeDetails: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var langugeStyle: UILabel!
    @IBOutlet weak var countryStyle: UILabel!
    @IBOutlet weak var typeStyle: UILabel!
    
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var imageContent: UIImageView!
    @IBOutlet weak var languageText: UILabel!
    @IBOutlet weak var countryText: UILabel!
    @IBOutlet weak var typeText: UILabel!
    
    @IBOutlet weak var summeryText: UITextView!
    
    @IBOutlet weak var texViewHeight: NSLayoutConstraint!
    // MARK: - Varaiables
    var tvHeight: CGFloat{
        summeryText.layoutIfNeeded()
        return summeryText.contentSize.height
    }
    var data: DataModel?
    init(data : DataModel) {
        self.data = data
        super.init(nibName: "HomeDetails", bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
        showData()
        getImages()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        texViewHeight.constant = tvHeight
    }

    fileprivate func style(){
        nameText.font = DesignSystem.TypoGraphy.title.font
        [languageText, countryText, typeText,langugeStyle,countryStyle,typeStyle].forEach{$0?.font = DesignSystem.TypoGraphy.mainText.font}
    }
    fileprivate func showData(){
        nameText.text = data?.show?.name
        languageText.text = data?.show?.language
        countryText.text = data?.show?.network?.country?.name ?? "unkown"
        typeText.text = data?.show?.type
        summeryText.attributedText = data?.show?.summary?.convertToAttributedString()
    }
    fileprivate func getImages(){
        guard let imageUrl = URL(string: data?.show?.image?.original ?? "") else { return}
        URLSession.shared.dataTask(with: imageUrl){ (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else {
                print("empty data")
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: data){
                    self.imageHeader.image = image
                    self.imageContent.image = image
                }
            }
        }.resume()
    }
}
