//
//  Home.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import UIKit

class Home: UIViewController {
    // MARK: - Outetls
    @IBOutlet weak var tableView: UITableView!
    // MARK: - Variables
    var apiNetwork = ApiService()
    var apiNetworkResponseData = [DataModel](){didSet{tableView.reloadData()}}
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        network()
    }
    fileprivate func setupTableView(){
        tableView.register(cell: HomeCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
    fileprivate func network(){
        apiNetwork.getData { (result) in
            print(result)
            switch result{
            case .success(let data):
                self.apiNetworkResponseData = data
            case .failure(let error):
                print("Error json \(error)")
            }
        }
    }
    

}
