//
//  TypoGraphy.swift
//  FontsHopeWork
//
//  Created by ziad on 09/10/2021.
//

import Foundation
import UIKit

extension DesignSystem{
    enum TypoGraphy{
        case title
        case mainText
        case subText
        case numbers

    
        private var fontDescriptor: CustomFontDescriptor{
            switch self {
            case .title:
                return CustomFontDescriptor(font: .bold, size: 25, style: .title1)
            case .mainText:
                return CustomFontDescriptor(font: .bold, size: 17, style: .body)
            case .subText:
                return CustomFontDescriptor(font: .regular, size: 14, style: .body)
            case .numbers:
                return CustomFontDescriptor(font: .regular, size: 12, style: .body)
            }
        }
        //Dynamic Font
        var font: UIFont{
            guard let font = UIFont(name: fontDescriptor.font.name, size: fontDescriptor.size) else { return UIFont.preferredFont(forTextStyle: fontDescriptor.style) }
            let fontMetrices = UIFontMetrics(forTextStyle: fontDescriptor.style)
            return fontMetrices.scaledFont(for: font)
        }
    }
}
