//
//  exUiview.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import Foundation
import UIKit
extension UIView{
    @IBInspectable var cornerRadius : CGFloat{
        get{
            return CGFloat()
        }set{
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = true
        }
    }
    
    static var nib: UINib {
        return UINib(nibName: self.className, bundle: nil)
    }
}
extension UITableView {
    
    func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T else { fatalError("DequeueReusableCell failed while casting") }
        return cell
    }
    
    func register<T: UITableViewCell>(cell: T.Type) {
        register(cell.nib, forCellReuseIdentifier: cell.className)
    }
}
extension NSObject {
    
    class var className: String {
        return "\(self)"
    }
}

class gradientView: UIImageView{
    var topColor = UIColor.clear.cgColor
    var bottomColor = UIColor.black.cgColor
    override func layoutSubviews() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [
            topColor,
            bottomColor
        ]
        gradientLayer.locations = [0.5, 1]
        gradientLayer.frame = bounds
        layer.addSublayer(gradientLayer)
    }
}
