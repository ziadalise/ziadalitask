//
//  HomeCell.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import UIKit

class HomeCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var imageContent: UIImageView!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var premieredText: UILabel!
    @IBOutlet weak var rateText: UILabel!
    @IBOutlet weak var runTimeText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        style()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    fileprivate func style(){
        [nameText, runTimeText].forEach{$0?.font = DesignSystem.TypoGraphy.mainText.font}
        [premieredText, rateText].forEach{$0?.font = DesignSystem.TypoGraphy.numbers.font}
        
    }
    func configureCell(data: DataModel){
        nameText.text = data.show?.name
        premieredText.text = data.show?.premiered
        rateText.text = String(data.show?.rating?.average ?? 0)
        
        guard let runtime = data.show?.runtime else { return  }
        runTimeText.text = "\(runtime) minutes"
        
        guard let imageUrl = URL(string: data.show?.image?.original ?? "") else{return}
        self.imageContent.image = nil
        getImage(url: imageUrl)
        
    }
    
    fileprivate func getImage(url: URL){
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else {
                print("empty data")
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: data){
                    self.imageContent.image = image
                }
                    
            }
        }.resume()
    }
}
