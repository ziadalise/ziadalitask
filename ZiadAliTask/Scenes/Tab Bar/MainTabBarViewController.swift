//
//  MainTabBarViewController.swift
//  ZiadAliTask
//
//  Created by ziad on 23/12/2021.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setuTabBar()
        
    }
    fileprivate func setuTabBar(){
        let vc1 = UINavigationController(rootViewController: Home())
        let vc2 = UINavigationController(rootViewController: Home())
        let vc3 = UINavigationController(rootViewController: Home())
        let vc4 = UINavigationController(rootViewController: Home())
        
        
        
        vc1.tabBarItem.image = UIImage(systemName: "house")
        vc2.tabBarItem.image = UIImage(systemName: "play.circle")
        vc3.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        vc4.tabBarItem.image = UIImage(systemName: "arrow.down.to.line")
        
        vc1.title = "Home"
        vc2.title = "Coming Soon"
        vc3.title = "Top Search"
        vc4.title = "Downloads"
        
        
        
        tabBar.tintColor = .label
        
        setViewControllers([vc1, vc2, vc3, vc4], animated: true)
    }
    

}
