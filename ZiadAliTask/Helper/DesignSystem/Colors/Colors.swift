//
//  Colors.swift
//  FontsHopeWork
//
//  Created by ziad on 09/10/2021.
//

import Foundation
import UIKit

extension DesignSystem{
    enum Colors: String{
        case bg = "bg"
        case star = "star"
        case cards = "cards"
        case mainText = "mainText"
        case subText = "subText"

        var color: UIColor{
            switch self {
            case .bg:
                return UIColor(named: self.rawValue)!
            case .star:
                return UIColor(named: self.rawValue)!
            case .cards:
                return UIColor(named: self.rawValue)!
            case .mainText:
                return UIColor(named: self.rawValue)!
            case .subText:
                return UIColor(named: self.rawValue)!
            }
        }
    }
}
